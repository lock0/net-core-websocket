﻿namespace service
{
    public class BaseService
    {
        public readonly LiveBroadcastDataContext DbContext;

        public BaseService(LiveBroadcastDataContext dbContext)
        {
            DbContext = dbContext;
        }
        public void Update()
        {
            DbContext.SaveChanges();
        }
        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}
