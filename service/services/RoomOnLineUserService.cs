﻿using library.models;
using library.services;
using System;
using System.Linq;

namespace service.services
{
    public class RoomOnLineUserService : BaseService, IRoomOnLineUserService
    {
        public RoomOnLineUserService(LiveBroadcastDataContext dbContext) : base(dbContext)
        {
        }

        public RoomOnLineUser GetRoomOnLineUser(Guid id)
        {
            return DbContext.RoomOnLineUsers.FirstOrDefault(n => n.Id == id);
        }

        public IQueryable<RoomOnLineUser> GetRoomOnLineUsers()
        {
            return DbContext.RoomOnLineUsers;
        }

        public void Insert(RoomOnLineUser roomOnLineUser)
        {
            DbContext.RoomOnLineUsers.Add(roomOnLineUser);
            Update();
        }

        public void OffLine(string openId, Guid roomId)
        {
            var item = DbContext.RoomOnLineUsers.FirstOrDefault(
                n => n.WeChatUser.OpenId == openId && n.RoomId == roomId);
            if (item != null)
            {
                DbContext.RoomOnLineUsers.Remove(item);
                Update();
            }
        }

        public void OnLine(string openId, Guid roomId)
        {
            var weChatUser = DbContext.WeChatUsers.FirstOrDefault(n => n.OpenId == openId);

            if (weChatUser != null &&
                DbContext.RoomOnLineUsers.Any(n => n.WeChatUserId == weChatUser.Id && n.RoomId == roomId) == false)
            {
                DbContext.RoomOnLineUsers.Add(new RoomOnLineUser
                {
                    RoomId = roomId,
                    WeChatUserId = weChatUser.Id
                });
                Update();
            }
        }
    }
}