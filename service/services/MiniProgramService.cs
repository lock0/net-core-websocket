﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using library.services;
using Newtonsoft.Json;

namespace service.services
{
    public class MiniProgramService : IMiniProgramService
    {
        public dynamic GetWxaCodeUnlimit(string accessToken, dynamic content)
        {
            var url =
                $"https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token={accessToken}";
            var client = new HttpClient();
            var res = client.PostAsync(url,
                    new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json")).Result
                .Content.ReadAsByteArrayAsync().Result;
            return res;
        }

        public dynamic GetAccessToken(string appid, string secret)
        {
            var url =
                $"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={appid}&secret={secret}";
            return JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(url));
        }

        public dynamic GetTemplateList(string accessToken)
        {
            var url =
                $"https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={accessToken}";
            return JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(url));
        }

        public dynamic SendTemplateMessage(string accessToken, dynamic content)
        {
            var url =
                $"https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token={accessToken}";
            return JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpPost(url,
                JsonConvert.SerializeObject(content)));
        }
    }
}