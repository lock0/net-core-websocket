﻿using library.models;
using library.services;
using System;
using System.Linq;

namespace service.services
{
    public class RoomService : BaseService, IRoomService
    {
        public RoomService(LiveBroadcastDataContext dbContext) : base(dbContext)
        {
        }

        public Room GetRoom(Guid id)
        {
            return DbContext.Rooms.FirstOrDefault(n => n.Id == id);
        }

        public IQueryable<Room> GetRooms()
        {
            return DbContext.Rooms.Where(n => !n.IsDeleted);
        }

        public void Insert(Room room)
        {
            DbContext.Rooms.Add(room);
            Update();
        }
    }
}