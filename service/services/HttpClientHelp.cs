﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace service.services
{
    public static class HttpClientHelp
    {
        /// <summary>
        /// 模拟get请求
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string HttpGet(string url)
        {
            try
            {
                var serviceAddress = url;
                var request = (HttpWebRequest)WebRequest.Create(serviceAddress);
                request.Method = "GET";
                request.ContentType = "text/html;charset=UTF-8";
                var response = (HttpWebResponse)request.GetResponse();
                var strm = response.GetResponseStream();
                var sr = new StreamReader(strm, Encoding.UTF8);
                string line;
                var sb = new StringBuilder();
                while ((line = sr.ReadLine()) != null)
                {
                    sb.Append(line + Environment.NewLine);
                }
                sr.Close();
                strm.Close();
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string SHA1_Hash(string strSha1In)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            var bytesSha1In = System.Text.Encoding.Default.GetBytes(strSha1In);
            var bytesSha1Out = sha1.ComputeHash(bytesSha1In);
            var strSha1Out = BitConverter.ToString(bytesSha1Out);
            strSha1Out = strSha1Out.Replace("-", "").ToLower();
            return strSha1Out;
        }
        public static string HttpPost(string postUrl, string postdata)
        {
            try
            {
                var cc = new CookieContainer();
                var request = (HttpWebRequest)WebRequest.Create(postUrl);
                request.Method = "Post";
                request.ContentType = "application/json;charset=UTF-8";
                var postdatabyte = Encoding.UTF8.GetBytes(postdata);
                request.ContentLength = postdatabyte.Length;
                request.AllowAutoRedirect = false;
                request.CookieContainer = cc;
                request.KeepAlive = true;

                //提交请求
                var stream = request.GetRequestStream();
                stream.Write(postdatabyte, 0, postdatabyte.Length);
                stream.Close();

                //接收响应
                var response = (HttpWebResponse)request.GetResponse();
                response.Cookies = request.CookieContainer.GetCookies(request.RequestUri);

                //CookieCollection cook = response.Cookies;
                ////Cookie字符串格式
                //string strcrook = request.CookieContainer.GetCookieHeader(request.RequestUri);
                var strm = response.GetResponseStream();

                var sr = new StreamReader(strm, Encoding.UTF8);

                string line;

                var sb = new StringBuilder();

                while ((line = sr.ReadLine()) != null)
                {
                    sb.Append(line + Environment.NewLine);
                }
                sr.Close();
                strm.Close();
                return sb.ToString();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="url"></param>
        /// <param name="filebytes"></param>
        /// <returns></returns>
        public static string HttpUpload(string url, byte[] filebytes)
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            var cookieContainer = new CookieContainer();
            request.CookieContainer = cookieContainer;
            request.AllowAutoRedirect = true;
            request.Method = "POST";
            var boundary = DateTime.Now.Ticks.ToString("X"); // 随机分隔线
            request.ContentType = "multipart/form-data;charset=utf-8;boundary=" + boundary;
            var itemBoundaryBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            var endBoundaryBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            /*int pos = path.LastIndexOf("\\");
            string fileName = path.Substring(pos + 1);*/
            //请求头部信息 
            var sbHeader = new StringBuilder(
                $"Content-Disposition:form-data;name=\"media\";filename=\"{Guid.NewGuid() + ".jpg"}\"\r\nContent-Type:application/octet-stream\r\n\r\n");
            var postHeaderBytes = Encoding.UTF8.GetBytes(sbHeader.ToString());
            var postStream = request.GetRequestStream();
            postStream.Write(itemBoundaryBytes, 0, itemBoundaryBytes.Length);
            postStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
            postStream.Write(filebytes, 0, filebytes.Length);
            postStream.Write(endBoundaryBytes, 0, endBoundaryBytes.Length);
            postStream.Close();
            //发送请求并获取相应回应数据
            var response = request.GetResponse() as HttpWebResponse;
            var instream = response.GetResponseStream();
            var sr = new StreamReader(instream, Encoding.UTF8);
            var content = sr.ReadToEnd();
            return content;
        }
    }
}
