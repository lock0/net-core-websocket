﻿using library.models;
using library.services;
using System;
using System.Linq;

namespace service.services
{
    public class LogService : BaseService, ILogService
    {
        public LogService(LiveBroadcastDataContext dbContext) : base(dbContext)
        {
        }

        public Log GetLog(Guid id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Log> GetLogs()
        {
            throw new NotImplementedException();
        }

        public void Insert(Log log)
        {
            DbContext.Logs.Add(log);
            Update();
        }
    }
}