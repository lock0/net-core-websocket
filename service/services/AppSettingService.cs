﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using library.models;
using library.services;

namespace service.services
{
    public class AppSettingService : BaseService, IAppSettingService
    {
        private readonly IMiniProgramService _miniProgramService;
        public AppSettingService(LiveBroadcastDataContext dbContext, IMiniProgramService miniProgramService) : base(dbContext)
        {
            _miniProgramService = miniProgramService;
        }

        public IQueryable<AppSetting> GetAppSettings()
        {
            return DbContext.AppSettings;
        }

        public void Delete(Guid id)
        {
            DbContext.AppSettings.Remove(GetAppSetting(id));
            Update();
        }

        public AppSetting GetAppSetting(Guid id)
        {
            return DbContext.AppSettings.FirstOrDefault(n => n.Id == id);
        }

        public void Insert(AppSetting appSetting)
        {
            DbContext.AppSettings.Add(appSetting);
            Update();
        }

        public AppSetting GetAppSettingIncludeLastToken(Guid id)
        {
            var item = GetAppSetting(id);
            if (item != null)
            {
                //处理AccessToken为空或者过期
                if (item.MiniProgramAccessToken == null || (item.MiniProgramAccessTokenExpireTime - DateTime.Now)?.TotalSeconds < 100)
                {
                    var info = _miniProgramService.GetAccessToken(item.MiniProgramAppId, item.MiniProgramSecret);

                    item.MiniProgramAccessToken = info.access_token.ToString();
                    item.MiniProgramAccessTokenExpireTime = DateTime.Now.AddSeconds(Convert.ToInt32(info.expires_in));
                    Update();
                }
            }

            return item;
        }


        public AppSetting GetAppSettingIncludeLastToken(string miniProgramAppId)
        {
            var item = GetAppSettings().FirstOrDefault(n => n.MiniProgramAppId == miniProgramAppId);
            if (item != null)
            {
                //处理AccessToken为空或者过期
                if (item.MiniProgramAccessToken == null || (item.MiniProgramAccessTokenExpireTime - DateTime.Now)?.TotalSeconds < 100)
                {
                    var info = _miniProgramService.GetAccessToken(item.MiniProgramAppId, item.MiniProgramSecret);

                    item.MiniProgramAccessToken = info.access_token.ToString();
                    item.MiniProgramAccessTokenExpireTime = DateTime.Now.AddSeconds(Convert.ToInt32(info.expires_in));
                    Update();
                }
            }

            return item;
        }
    }
}
