﻿using library.models;
using library.services;
using System;
using System.Linq;

namespace service.services
{
    public class WeChatUserService : BaseService, IWeChatUserService
    {
        public WeChatUserService(LiveBroadcastDataContext dbContext) : base(dbContext)
        {
        }

        public WeChatUser GetWeChatUser(Guid id)
        {
            return DbContext.WeChatUsers.FirstOrDefault(n => n.Id == id);
        }

        public WeChatUser GetWeChatUser(string openId)
        {
            return DbContext.WeChatUsers.FirstOrDefault(n => n.OpenId == openId);
        }

        public WeChatUser GetWeChatUserByTicket(string ticket)
        {
            return GetWeChatUsers().FirstOrDefault(n => n.Ticket == ticket);
        }

        public IQueryable<WeChatUser> GetWeChatUsers()
        {
            return DbContext.WeChatUsers.Where(n => !n.IsDeleted);
        }

        public void Insert(WeChatUser weChatUser)
        {
            DbContext.WeChatUsers.Add(weChatUser);
            Update();
        }
    }
}
