﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using library.interfaces;
using library.models;
using library.services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace service
{
    public class LiveBroadcastDataContext : DbContext, IDataContext
    {
        public DbSet<AppSetting> AppSettings { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<WeChatUser> WeChatUsers { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomOnLineUser> RoomOnLineUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // 老数据库不支持分页 2008 及以下
            // UseLazyLoadingProxies 懒加载，否则导航属性会null
            optionsBuilder
                .UseLazyLoadingProxies().UseSqlServer(GetConnctionString(), n => n.UseRowNumberForPaging());
        }
        public static string GetConnctionString()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            var enString = configuration.GetConnectionString("DefaultConnection");
            var key = configuration.GetSection("DecryptKey").Value;
            var connString = Decrypt(enString, key);
            return connString;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*modelBuilder.Entity<SalesPosition>().HasMany(c => c.SalesPositionReportLines)
                .WithOne(p => p.SalesPosition)
                .HasForeignKey(p => p.SalesPositionId);*/
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="encString">密文</param>
        /// <param name="key">密钥</param>
        /// <returns></returns>
        private static string Decrypt(string encString, string key)
        {
            var rgbKey = new byte[8];
            var rgbIv = new byte[8];
            var des = new DESCryptoServiceProvider();
            var hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(key));
            Array.Copy(hash, 0, rgbKey, 0, 8);
            Array.Copy(hash, 8, rgbIv, 0, 8);
            try
            {
                using (var ms = new MemoryStream(Convert.FromBase64String(encString)))
                {
                    using (var cs = new CryptoStream(ms,
                        des.CreateDecryptor(rgbKey, rgbIv), CryptoStreamMode.Read))
                    {
                        using (var sr = new StreamReader(cs))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries<IDtStamped>();

            foreach (var dtStamped in entities)
            {
                if (dtStamped.State == EntityState.Added)
                {
                    dtStamped.Entity.CreatedTime = DateTime.Now;
                    dtStamped.Entity.UpdateTime = DateTime.Now;
                }

                if (dtStamped.State == EntityState.Modified)
                {
                    dtStamped.Entity.UpdateTime = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}
