﻿using System.Collections.Generic;
using WebSocket.models;

namespace WebSocket.infrastructure.helper
{
    public static class SmsHelper
    {
        public static string SendSms(SmsSettingModel smsSetting, string phone,string templateValue)
        {
            // *** 需用户填写部分 ***

            //fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息

            string accessKeyId = smsSetting.AliyunAppId; //你的accessKeyId，参考本文档步骤2
            string accessKeySecret = smsSetting.AliyunAppSecret; //你的accessKeySecret，参考本文档步骤2

            Dictionary<string, string> smsDict = new Dictionary<string, string>();

            //fixme 必填: 短信接收号码
            smsDict.Add("PhoneNumbers", phone);

            //fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
            smsDict.Add("SignName", smsSetting.SmsSignName);

            //fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template

            smsDict.Add("TemplateCode", smsSetting.SmsTemplateCode);

            // fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
            smsDict.Add("TemplateParam", templateValue);

            //什么？Newtonsoft.Json也觉得重，那拼字符串好了
            //smsDict.Add("TemplateParam", "{\"appname\":\"微关爱\",\"appstorename\":\"小黑\"}");


            // *** 以下内容无需修改 ***
            smsDict.Add("RegionId", "cn-hangzhou");
            smsDict.Add("Action", "SendSms");
            smsDict.Add("Version", "2017-05-25");

            string domain = "dysmsapi.aliyuncs.com"; //短信API产品域名（接口地址固定，无需修改）

            // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
            var singnature = new SignatureHelper();

            return singnature.Request(accessKeyId, accessKeySecret, domain, smsDict);
        }
    }
}