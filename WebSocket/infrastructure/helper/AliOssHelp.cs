﻿using System.IO;
using Aliyun.OSS;

namespace WebSocket.infrastructure.helper
{
    public static class AliOssHelp
    {
        /// <summary>
        /// 上传文件
        /// </summary>
        public static void Upload(string endpoint, string accessKeyId, string accessKeySecret, string bucketName, string objectName, byte[] binaryData)
        {
            // 创建OssClient实例。
            var client = new OssClient(endpoint, accessKeyId, accessKeySecret);
            var requestContent = new MemoryStream(binaryData);
            // 上传文件。
            client.PutObject(bucketName, objectName, requestContent);
        }
    }
}