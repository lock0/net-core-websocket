﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace WebSocket.infrastructure.helper
{
    /// <summary>
    /// 小程序解密
    /// </summary>
    public static class DecryptHelp
    {
        public static string aes_decrypt(string encryptedDataStr, string key, string iv)
        {
            var rijalg = new RijndaelManaged
            {
                //-----------------    
                //设置 cipher 格式 AES-128-CBC    

                KeySize = 128,
                Padding = PaddingMode.PKCS7,
                Mode = CipherMode.CBC,
                Key = Convert.FromBase64String(key),
                IV = Convert.FromBase64String(iv)
            };

            byte[] encryptedData = Convert.FromBase64String(encryptedDataStr);
            //解密    
            var decryptor = rijalg.CreateDecryptor(rijalg.Key, rijalg.IV);
            string result;
            using (var msDecrypt = new MemoryStream(encryptedData))
            {
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (var srDecrypt = new StreamReader(csDecrypt))
                    {
                        result = srDecrypt.ReadToEnd();
                    }
                }
            }
            return result;
        }
    }
}