﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WebSocket.infrastructure.exceptions
{
    public static class Extensions
    {
        /// <summary>
        /// //时间戳从1970年1月1日00:00:00至今的秒数,即当前的时间
        /// </summary>
        /// <returns></returns>
        public static long GetUnixTime()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        public static int? ToInt(this string str)
        {
            try
            {
                return Convert.ToInt32(str);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Guid ToGuidOrDefault(this string str)
        {
            try
            {
                return new Guid(str);
            }
            catch (Exception)
            {
                return Guid.Empty;
            }
        }

        public static bool? ToBool(this string str)
        {
            try
            {
                return Convert.ToBoolean(str);
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// 字符串MD5加密
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string Md5Encryption(this string context)
        {
            return BitConverter.ToString(MD5.Create().ComputeHash(Encoding.Default.GetBytes(context)))
                .Replace("-", "");
        }

        /// <summary>
        /// sha256加密
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string Sha256Encryption(this string context)
        {
            var sha256Data = Encoding.Default.GetBytes(context);
            var sha256 = new SHA256Managed();
            var result = sha256.ComputeHash(sha256Data);
            return BitConverter.ToString(result).Replace("-", "");
        }

        /// <summary>
        /// Base64加密
        /// </summary>
        /// <param name="context">需要加密的字符串</param>
        /// <returns>返回加密的结果</returns>
        public static string Base64Encryption(this string context)
        {
            return Convert.ToBase64String(Encoding.Default.GetBytes(context));
        }

        /// <summary>
        /// Base64解密
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string Base64Decryption(this string context)
        {
            return Encoding.Default.GetString(Convert.FromBase64String(context));
        }
    }
}