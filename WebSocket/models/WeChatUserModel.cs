﻿using System;

namespace WebSocket.models
{
    public class WeChatUserModel
    {
        public Guid Id { get; set; }
        public string UnionId { get; set; }
        public string OpenId { get; set; }
        public string NickName { get; set; }
        public string AvatarUrl { get; set; }
        public string Ticket { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}