﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    /// <summary>
    /// 资料
    /// </summary>
    public class DocumentModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Urls { get; set; }
    }
}