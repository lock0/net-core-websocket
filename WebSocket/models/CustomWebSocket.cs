﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class CustomWebSocket
    {
        public string RoomId { get; set; }
        public WebSocketUser User { get; set; }
        public System.Net.WebSockets.WebSocket WebSocket { get; set; }
    }

    public class WebSocketUser
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}
