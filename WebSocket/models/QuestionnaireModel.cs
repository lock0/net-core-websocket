﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    /// <summary>
    /// 问卷
    /// </summary>
    public class QuestionnaireModel
    {
        public string Id { get; set; }
        /// <summary>
        /// 问卷标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 问卷URL
        /// </summary>
        public string Url { get; set; }
    }
}