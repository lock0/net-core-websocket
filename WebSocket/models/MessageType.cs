﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    /// <summary>
    /// 格式化信息,进入房间
    /// </summary>
    public class MessageJoinFormat
    {
        public WebSocketUser User { get; set; }
        public readonly MessageType Type;

        public MessageJoinFormat()
        {
            this.Type = MessageType.Join;
        }
    }

    /// <summary>
    /// 格式化信息,离开房间
    /// </summary>
    public class MessageLeaveFormat
    {
        public WebSocketUser User { get; set; }
        public readonly MessageType Type;

        public MessageLeaveFormat()
        {
            this.Type = MessageType.Leave;
        }
    }

    /// <summary>
    /// 其他
    /// </summary>
    public class MessageOtherFormat
    {
        public string Context { get; set; }
        public readonly MessageType Type;

        public MessageOtherFormat()
        {
            this.Type = MessageType.Other;
        }
    }

    public enum MessageType
    {
        Other, //客户端普通交互
        Join, //系统级别人员加入
        Leave,//系统给级别人员离开
    }
}