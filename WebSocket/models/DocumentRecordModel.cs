﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class DocumentRecordModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }
}
