﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class AppSettingModel
    {
        public string MiniProgramAppId { get; set; }
        public string MiniProgramSecret { get; set; }
        /// <summary>
        /// 微信商户Id
        /// </summary>
        public string MiniProgramMchId { get; set; }
        /// <summary>
        /// 外网IP
        /// </summary>
        public string ExtranetIp { get; set; }
        /// <summary>
        /// 微信支付API调用密钥
        /// </summary>
        public string WeXinPaymentApiSecret { get; set; }
    }
}
