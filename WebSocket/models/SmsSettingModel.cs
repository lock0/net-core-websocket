﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class SmsSettingModel
    {
        /// <summary>
        /// 阿里云
        /// </summary>
        public string AliyunAppId { get; set; }
        public string AliyunAppSecret { get; set; }
        /// <summary>
        /// 短信签名
        /// </summary>
        public string SmsSignName { get; set; }
        /// <summary>
        /// 短信模板Code
        /// </summary>
        public string SmsTemplateCode { get; set; }
        /// <summary>
        /// 随机数长度
        /// </summary>
        public int? RandomNumberLength { get; set; }
        /// <summary>
        /// 验证码获取最大间隔秒数
        /// </summary>
        public int? IntervalTime { get; set; }
        /// <summary>
        /// 验证码过期秒数
        /// </summary>
        public int? ExpiryTime { get; set; }
    }
}
