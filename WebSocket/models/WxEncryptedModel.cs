﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    /// <summary>
    /// 微信加密信息
    /// </summary>
    public class WxEncryptedModel
    {
        /// <summary>
        /// 包括敏感数据在内的完整用户信息的加密数据，详细见加密数据解密算法
        /// </summary>
        public string encryptedData { get; set; }
        /// <summary>
        /// 加密算法的初始向量，详细见加密数据解密算法
        /// </summary>
        public string iv { get; set; }
        public string sessionkey { get; set; }
    }
}
