﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class RoomModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }

        /// <summary>
        /// 主持人
        /// </summary>
        public string HostWeChatUserUnionId { get; set; }

        /// <summary>
        /// 推流地址
        /// </summary>
        public string PushUrl { get; set; }

        /// <summary>
        /// 播放地址
        /// </summary>
        public string PlayUrl { get; set; }

        /// <summary>
        /// 摘要图片
        /// </summary>
        public string AbstractImgUrl { get; set; }

        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 直播开始时间
        /// </summary>
        public DateTime? StartDateTime { get; set; }

        /// <summary>
        /// 会议状态
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 直播结束时间
        /// </summary>
        public DateTime? EndDateTime { get; set; }

        /// <summary>
        /// 当前播放文档的Id
        /// </summary>
        public string DocumentRecordId { get; set; }

        /// <summary>
        /// 当前播放文档的URL
        /// </summary>
        public string DocumentRecordUrl { get; set; }
    }
}