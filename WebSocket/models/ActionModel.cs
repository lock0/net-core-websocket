﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class ActionModel
    {
        /// <summary>
        /// 动作：start | close
        /// </summary>
        public string Action { get; set; }
    }
}
