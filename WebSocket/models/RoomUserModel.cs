﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class RoomUserModel
    {
        /// <summary>
        /// RoomUserId
        /// </summary>
        public Guid Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// 是否是当前讲者
        /// </summary>
        public bool IsSpeaker { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string AvatarUrl { get; set; }
    }
}