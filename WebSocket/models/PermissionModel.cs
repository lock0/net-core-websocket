﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    /// <summary>
    /// 权限
    /// </summary>
    public class PermissionModel
    {
        public Guid RoomUserId { get; set; }
        /// <summary>
        /// 是否是主讲人
        /// </summary>
        public bool IsSpeaker { get; set; }
        /// <summary>
        /// 是否是主持人
        /// </summary>
        public bool IsHost { get; set; }
    }
}
