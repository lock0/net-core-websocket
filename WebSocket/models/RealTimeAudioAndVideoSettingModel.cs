﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSocket.models
{
    public class RealTimeAudioAndVideoSettingModel
    {
        /// <summary>
        /// 默认为WeChatUserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 实时音视频的SdkAppId
        /// </summary>
        public string SdkAppId { get; set; }
        /// <summary>
        /// 实时音视频签名
        /// </summary>

        public string UserSig { get; set; }
    }
}