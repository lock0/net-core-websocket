﻿using library.models;
using library.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using WebSocket.infrastructure.exceptions;
using WebSocket.infrastructure.helper;
using WebSocket.models;

namespace WebSocket.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : BaseController
    {
        private readonly IAppSettingService _appSettingService;
        private readonly IWeChatUserService _weChatUserService;
        private readonly IRoomService _roomService;
        private readonly IMiniProgramService _miniProgramService;
        private readonly IRoomOnLineUserService _roomOnLineUserService;

        public RoomController(IAppSettingService appSettingService,
            IRoomService roomService,
            IRoomOnLineUserService roomOnLineUserService,
            IMiniProgramService miniProgramService,
            IWeChatUserService weChatUserService)
        {
            _appSettingService = appSettingService;
            _weChatUserService = weChatUserService;
            _roomService = roomService;
            _roomOnLineUserService = roomOnLineUserService;
            _miniProgramService = miniProgramService;
        }

        /// <summary>
        /// 获取实时音视频的当前用户配置文件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRealTimeAudioAndVideoSetting")]
        public RealTimeAudioAndVideoSettingModel GetRealTimeAudioAndVideoSetting()
        {
            var ticket = User.Identity.Name; // wechatuser ticket
            var weChatUser = _weChatUserService.GetWeChatUserByTicket(ticket);
            var appSetting = _appSettingService.GetAppSettings().FirstOrDefault();
            return new RealTimeAudioAndVideoSettingModel
            {
                SdkAppId = appSetting?.RealTimeAudioAndVideoSdkAppId,
                UserId = weChatUser.OpenId,
                UserSig = weChatUser.Signature
            };
        }

        [HttpGet]
        [Route("AllowJoin/{roomCode}")]
        public bool AllowJoin(string roomCode)
        {
            var ticket = User.Identity.Name; // wechatuser ticket
            var weChatUser = _weChatUserService.GetWeChatUserByTicket(ticket);
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            var room = _roomService.GetRooms().FirstOrDefault(n => n.Code == roomCode);
            var apiUrl =
                $"{configuration.GetSection("ApiUrl").Value}api/RemoteCampaign/GetRemoteMeetingJurisdiction/{room?.Code}/{weChatUser.UnionId}/";
            var result = JsonConvert.DeserializeObject<bool>(HttpClientHelp.HttpGet(apiUrl));
            return result;
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRoomById/{id}")]
        public RoomModel GetRoom(Guid id)
        {
            var room = _roomService.GetRooms().Where(n => n.Id == id).Select(n => new RoomModel
            {
                Id = n.Id,
                Code = n.Code,
                UpdateTime = n.UpdateTime,
                AbstractImgUrl = n.AbstractImgUrl,
                StartDateTime = n.StartDateTime,
                EndDateTime = n.EndDateTime,
                State = n.State,
                DocumentRecordUrl = n.DocumentRecordUrl,
                DocumentRecordId = n.DocumentRecordId
            }).FirstOrDefault();
            //计算推流地址和播放地址
            var appSetting = _appSettingService.GetAppSettings().FirstOrDefault();
            if (appSetting != null && room != null)
            {
                //过期时间
                var txTime = DateTimeOffset.UtcNow.AddDays(1).ToUnixTimeSeconds();
                //防盗链签名
                var txSecret = $"{appSetting.PushSecretKey}{room.Code}{txTime}".Md5Encryption().ToLower();
                //拼接推流地址
                room.PushUrl = $"rtmp://{appSetting.PushDomain}/live/{room.Code}?txSecret={txSecret}&txTime={txTime}";
                /*room.PushUrl =
                    "rtmp://27921.livepush.myqcloud.com/live/abc?txSecret=66c6fadd2b93b30700a255b47b469a3e&txTime=5CC1D97F";*/
                //拼接播放地址 暂时没做盗链，盗链需要域名https支持
                room.PlayUrl = $"rtmp://{appSetting.PlayDomain}/live/{room.Code}";
            }

            return room;
        }

        /// <summary>
        /// 更新(开启或者关闭会议)
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="model"></param>
        [HttpPut]
        [Route("StartOrClose/{roomId}")]
        public void StartOrClose(Guid roomId, ActionModel model)
        {
            var room = _roomService.GetRoom(roomId);
            switch (model.Action)
            {
                case "start":
                    room.StartDateTime = DateTime.Now;
                    
                    break;
                case "close":
                    room.EndDateTime = DateTime.Now;
                    break;
            }

            _roomService.Update();
        }

        /// <summary>
        /// 更改会议状态
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="model"></param>
        [HttpPut]
        [Route("UpdateState/{roomId}")]
        public void UpdateState(Guid roomId, ActionModel model)
        {
            var room = _roomService.GetRoom(roomId);
            room.State = model.Action;
            _roomService.Update();
        }

        /// <summary>
        /// 记录文档播放记录
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="model"></param>
        [HttpPut]
        [Route("SaveDocumentRecord/{roomId}")]
        public void SaveDocumentRecord(Guid roomId, DocumentRecordModel model)
        {
            var room = _roomService.GetRoom(roomId);
            room.DocumentRecordId = model.Id;
            room.DocumentRecordUrl = model.Url;
            _roomService.Update();
        }

        [HttpGet]
        [Route("GetRoomByCode/{code}")]
        public RoomModel GetRoom(string code)
        {
            var id = _roomService.GetRooms().Where(n => n.Code == code).Select(n => n.Id).FirstOrDefault();
            return GetRoom(id);
        }

        /// <summary>
        /// 获取资料，这里是模拟数据，图片list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDocuments/{roomId}")]
        public DocumentModel[] GetDocuments(Guid roomId)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            var room = _roomService.GetRoom(roomId);
            var apiUrl =
                $"{configuration.GetSection("ApiUrl").Value}api/RemoteCampaign/GetRemoteMaterial/{room?.Code}";
            var results = JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(apiUrl));
            var list = new List<DocumentModel>();
            foreach (var item in results)
            {
                var urls = new List<string>();
                var doc = new DocumentModel
                {
                    Id = item.Id.ToString(),
                    Name = item.Name.ToString()
                };
                foreach (var url in item.Urls)
                {
                    urls.Add(url.ToString());
                }

                doc.Urls = urls.ToArray();
                list.Add(doc);
            }

            /*var list = new List<DocumentModel>
            {
                new DocumentModel
                {
                    Id = new Guid("E3A81F1E-A3D0-4BFC-95BD-B27ED010DE7F"),
                    Name = "文档1",
                    Urls = new[]
                    {
                        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556516197054&di=db6efbd35977319b93be0674057eb6de&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201507%2F07%2F20150707100822_zCAFQ.jpeg",
                        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556516246646&di=607b0790026858376628a4a722d87d3a&imgtype=0&src=http%3A%2F%2Fpic31.nipic.com%2F20130731%2F13345615_081736258119_2.jpg"
                    }
                },
                new DocumentModel
                {
                    Id = new Guid("0068FFD1-3E49-45C2-8611-EC54D8433933"),
                    Name = "文档2",
                    Urls = new[]
                    {
                        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556517396616&di=5b7e10bbd8d6f7c66ab89ef560ab413f&imgtype=0&src=http%3A%2F%2Fk.zol-img.com.cn%2Fdcbbs%2F11417%2Fa11416438_01000.jpg",
                        "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1556517428909&di=f7f1a738734c7e994332a2b39ea2e7ad&imgtype=0&src=http%3A%2F%2Fg2.hexunimg.cn%2F2014-09-24%2F168807657.jpg"
                    }
                }
            };*/
            return list.ToArray();
        }

        /// <summary>
        /// 开房间
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateRoom")]
        public ResponseModel CreateRoom(RoomModel model)
        {
            if (!_roomService.GetRooms().Any(n => n.Code == model.Code))
            {
                _roomService.Insert(new Room
                {
                    Id = Guid.NewGuid(),
                    Code = model.Code,
                    AbstractImgUrl = model.AbstractImgUrl,
                    HostWeChatUserUnionId = model.HostWeChatUserUnionId
                });
                return Success();
            }

            var room = _roomService.GetRooms().FirstOrDefault(n => n.Code == model.Code);
            if (room == null)
            {
                return Failed("找不到会议");
            }
            room.AbstractImgUrl = model.AbstractImgUrl;
            room.HostWeChatUserUnionId = model.HostWeChatUserUnionId;
            _roomService.Update();
            return Success();
        }

        /// <summary>
        /// 用户进入房间，记录用户
        /// </summary>
        /// 
        /// <param name="roomId"></param>
        /// <param name="model"></param>
        [HttpPost]
        [Route("JoinRoom/{roomId}")]
        public void JoinRoom(Guid roomId, WeChatUserModel model)
        {
            var ticket = User.Identity.Name; // wechatuser ticket
            var weChatUser = _weChatUserService.GetWeChatUserByTicket(ticket);
            var room = _roomService.GetRoom(roomId);
            if (weChatUser != null && room != null)
            {
                if (room.RoomOnLineUsers.All(n => n.WeChatUserId != weChatUser.Id))
                {
                    room.RoomOnLineUsers.Add(new RoomOnLineUser
                    {
                        WeChatUserId = weChatUser.Id
                    });
                    _roomService.Update();
                }

                //更新wechatuser的信息，头像 昵称等
                weChatUser.AvatarUrl = model.AvatarUrl;
                weChatUser.NickName = model.NickName;
                _weChatUserService.Update();
            }
        }

        /// <summary>
        /// 获取房间人员信息
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRoomUsers/{roomId}")]
        public RoomUserModel[] GetRoomUsers(Guid roomId)
        {
            return _roomService.GetRoom(roomId).RoomOnLineUsers.Select(n => new RoomUserModel
            {
                Id = n.Id,
                Name = n.WeChatUser.NickName, // 前端显示的名字，可以随时调整真实名字
                IsSpeaker = n.IsSpeaker,
                AvatarUrl = n.WeChatUser.AvatarUrl
            }).ToArray();
        }

        /// <summary>
        /// 传麦
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [Route("SetSpeaker")]
        public void SetSpeaker(RoomUserModel model)
        {
            var roomUser = _roomOnLineUserService.GetRoomOnLineUser(model.Id);
            if (roomUser != null)
            {
                var ticket = User.Identity.Name; // wechatuser ticket
                var weChatUser = _weChatUserService.GetWeChatUserByTicket(ticket);
                //判断主持人权限
                if (roomUser.Room.HostWeChatUserId == weChatUser.Id)
                {
                    var speakers = roomUser.Room.RoomOnLineUsers.Where(n => n.IsSpeaker).ToArray();
                    foreach (var speaker in speakers)
                    {
                        speaker.IsSpeaker = false;
                    }

                    roomUser.IsSpeaker = true;
                    _roomOnLineUserService.Update();
                }
            }
        }

        /// <summary>
        /// 获取预置的讲者列表
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAdvanceSpeakers/{roomId}")]
        public RoomUserModel[] GetAdvanceSpeakers(Guid roomId)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            var room = _roomService.GetRoom(roomId);
            var apiUrl =
                $"{configuration.GetSection("ApiUrl").Value}api/RemoteCampaign/GetAdvanceSpeakers/{room?.Code}";
            var results = JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(apiUrl));
            var unionIds = new List<string>();
            foreach (string unionId in results)
            {
                if (!string.IsNullOrEmpty(unionId))
                {
                    unionIds.Add(unionId);
                }

            }

            var ids = unionIds.ToArray();
            var models = _roomOnLineUserService.GetRoomOnLineUsers()
                .Where(n => n.RoomId == room.Id && ids.Contains(n.WeChatUser.UnionId)).Select(n => new RoomUserModel
                {
                    Id = n.Id,
                    Name = n.WeChatUser.NickName, // 前端显示的名字，可以随时调整真实名字
                    IsSpeaker = n.IsSpeaker,
                    AvatarUrl = n.WeChatUser.AvatarUrl,
                }).ToArray();
            return models;
        }

        /// <summary>
        /// 获取我的权限状态
        /// </summary>
        [HttpGet]
        [Route("GetPermissionStatus/{roomId}")]
        public PermissionModel GetPermissionStatus(Guid roomId)
        {
            var permission = new PermissionModel();
            var ticket = User.Identity.Name; // wechatuser ticket
            var weChatUser = _weChatUserService.GetWeChatUserByTicket(ticket);
            var room = _roomService.GetRoom(roomId);
            if (room != null && weChatUser != null)
            {
                permission.RoomUserId = room.RoomOnLineUsers.Where(n => n.WeChatUserId == weChatUser.Id)
                    .Select(n => n.Id)
                    .FirstOrDefault();
                permission.IsSpeaker =
                    room.RoomOnLineUsers.Any(n => n.IsSpeaker == true && n.WeChatUserId == weChatUser.Id);
                permission.IsHost = room.HostWeChatUserUnionId == weChatUser.UnionId;
            }

            return permission;
        }

        /// <summary>
        /// 获取小程序二维码B类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetWxaCodeUnLimit/{roomCode}")]
        public dynamic GetWxaCodeUnLimit(string roomCode)
        {
            var model = new MiniCodeUnLimitModel
            {
                scene = roomCode
            };
            var ticket = this.User.Identity.Name; //这个时候ticket是MiniProgramAppId
            var appSetting = _appSettingService.GetAppSettingIncludeLastToken(ticket);
            var res = _miniProgramService.GetWxaCodeUnlimit(appSetting.MiniProgramAccessToken, model);
            //如果调用成功，会直接返回图片二进制内容，如果请求失败，会返回 JSON 格式的数据。
            return res;
        }

        /// <summary>
        /// 获取问卷列表
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQuestionnaires/{roomId}")]
        public QuestionnaireModel[] GetQuestionnaires(Guid roomId)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            var room = _roomService.GetRoom(roomId);
            var apiUrl =
                $"{configuration.GetSection("ApiUrl").Value}api/RemoteCampaign/GetQuestionnaires/{room?.Code}";
            var results = JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(apiUrl));
            var list = new List<QuestionnaireModel>();
            foreach (var item in results)
            {
                var q = new QuestionnaireModel
                {
                    Id = item.Id.ToString(),
                    Title = item.Title.ToString(),
                    Url = item.Title.ToString()
                };
                list.Add(q);
            }

            return list.ToArray();
        }
    }
}