﻿using library.services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using library.models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using tencentyun;
using WebSocket.infrastructure.helper;
using WebSocket.models;

namespace WebSocket.Controllers
{
    /// <summary>
    /// jwt服务
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class JwtController : ControllerBase
    {
        private readonly IAppSettingService _appSettingService;
        private readonly IWeChatUserService _weChatUserService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public JwtController(IAppSettingService appSettingService,
            IWeChatUserService weChatUserService,
            IHostingEnvironment hostingEnvironment)
        {
            _appSettingService = appSettingService;
            _weChatUserService = weChatUserService;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// 获取client的JWT
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetClientToken")]
        public IActionResult GetClientToken(AppSettingModel model)
        {
            var item = _appSettingService.GetAppSettings()
                .FirstOrDefault(n =>
                    n.MiniProgramAppId == model.MiniProgramAppId && n.MiniProgramSecret == model.MiniProgramSecret);
            if (item != null)
            {
                var newJwtToken = GenerateJwtSecurityToken(model.MiniProgramAppId);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(newJwtToken),
                    expires_in = newJwtToken.Payload.Exp
                });
            }

            return Unauthorized("Could not verify username and password");
        }

        [HttpPost]
        [Authorize]
        [Route("GetWeChatUserToken")]
        public IActionResult GetWeChatUserToken(WeChatUserModel model)
        {
            var appSetting = _appSettingService.GetAppSettings().FirstOrDefault();
            var weChatUser = _weChatUserService.GetWeChatUser(model.OpenId);
            //获取实时音视频签名
            var priKeyContent = GetPriKey();
            var pubKeyContent = GetPubKey();
            var api = new TLSSigAPI(Convert.ToInt32(appSetting?.RealTimeAudioAndVideoSdkAppId), priKeyContent,
                pubKeyContent);
            if (weChatUser == null)
            {
                weChatUser = new WeChatUser
                {
                    Id = Guid.NewGuid(),
                    AvatarUrl = model.AvatarUrl,
                    NickName = model.NickName,
                    OpenId = model.OpenId,
                    UnionId = model.UnionId,
                    Ticket = Guid.NewGuid().ToString()
                };
                var sig = api.genSig(weChatUser.OpenId);
                weChatUser.Signature = sig;
                _weChatUserService.Insert(weChatUser);
            }
            else
            {
                weChatUser.Ticket = Guid.NewGuid().ToString();
                var sig = api.genSig(weChatUser.OpenId);
                weChatUser.Signature = sig;
                _weChatUserService.Update();
            }

            var newJwtToken = GenerateJwtSecurityToken(weChatUser.Ticket);
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(newJwtToken),
                expires_in = newJwtToken.Payload.Exp
            });
        }

        [HttpPost]
        [Route("AddWeChatUser")]
        public void AdWeChatUser(WxEncryptedModel model)
        {
            var data = DecryptHelp.aes_decrypt(model.encryptedData, model.sessionkey, model.iv);
            var json = JsonConvert.DeserializeObject<dynamic>(data);
            string unionId = json.unionId.ToString();
            string openId = json.openId.ToString();
            if (!string.IsNullOrEmpty(unionId))
            {
                var weChatUser = _weChatUserService.GetWeChatUsers().FirstOrDefault(n => n.OpenId == openId);
                if (weChatUser != null)
                {
                    weChatUser.AvatarUrl = json.avatarUrl.ToString();
                    weChatUser.NickName = json.nickName.ToString();
                    weChatUser.UnionId = json.unionId.ToString();
                    _weChatUserService.Update();
                }
            }
        }


        private JwtSecurityToken GenerateJwtSecurityToken(string ticket)
        {
            var claims = new List<Claim> {new Claim(ClaimTypes.Name, ticket)};
            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            var jwtSetting = configuration.GetSection("JwtSetting");
            var issuer = jwtSetting["Issuer"];
            var audience = jwtSetting["Audience"];
            var symmetricSecurityKey = jwtSetting["SymmetricSecurityKey"];
            var expiresIn = Convert.ToInt32(jwtSetting["ExpiresIn"]);
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(symmetricSecurityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims.ToArray(),
                expires: DateTime.Now.AddSeconds(expiresIn),
                signingCredentials: creds);
            return token;
        }

        /// <summary>
        /// 获取实时音视频私钥
        /// </summary>
        /// <returns></returns>
        private string GetPriKey()
        {
            var contentRootPath = _hostingEnvironment.ContentRootPath;
            var priKeyPath = contentRootPath + "\\keys\\private_key.txt";
            var f = new FileStream(priKeyPath, FileMode.Open, FileAccess.Read);
            var reader = new BinaryReader(f);
            var b = new byte[f.Length];
            reader.Read(b, 0, b.Length);
            var priKey = Encoding.Default.GetString(b);
            return priKey;
        }

        /// <summary>
        /// 获取实时音视频公钥
        /// </summary>
        /// <returns></returns>
        private string GetPubKey()
        {
            var contentRootPath = _hostingEnvironment.ContentRootPath;
            var priKeyPath = contentRootPath + "\\keys\\public_key.txt";
            var f = new FileStream(priKeyPath, FileMode.Open, FileAccess.Read);
            var reader = new BinaryReader(f);
            var b = new byte[f.Length];
            reader.Read(b, 0, b.Length);
            var pubKey = Encoding.Default.GetString(b);
            return pubKey;
        }
    }
}