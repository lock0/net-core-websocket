﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using library.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebSocket.infrastructure.helper;
using WebSocket.models;
using HttpClientHelp = service.services.HttpClientHelp;

namespace WebSocket.Controllers
{
    /// <summary>
    /// 与微信服务器的交互
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiveController : ControllerBase
    {
        private readonly IAppSettingService _appSettingService;
        private readonly IMiniProgramService _miniProgramService;

        public ReceiveController(IAppSettingService appSettingService, IMiniProgramService miniProgramService)
        {
            _appSettingService = appSettingService;
            _miniProgramService = miniProgramService;
        }

        [HttpGet]
        [Route("Code2Session/{code}")]
        public dynamic GetCode2Session(string code)
        {
            var ticket = this.User.Identity.Name; //这个时候ticket是MiniProgramAppId

            var app = _appSettingService.GetAppSettings().FirstOrDefault(n =>
                n.MiniProgramAppId == ticket);
            var url =
                $"https://api.weixin.qq.com/sns/jscode2session?appid={app?.MiniProgramAppId}&secret={app?.MiniProgramSecret}&js_code={code}&grant_type=authorization_code";
            return JsonConvert.DeserializeObject<dynamic>(HttpClientHelp.HttpGet(url));
        }
    }
}