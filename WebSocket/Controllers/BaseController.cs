﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebSocket.models;

namespace WebSocket.Controllers
{
    [EnableCors("CorsPolicy")]
    public class BaseController : ControllerBase
    {
        protected ResponseModel Success(string msg = null)
        {
            return new ResponseModel
            {
                Message = string.IsNullOrEmpty(msg) ? "success" : msg,
                Error = false
            };
        }

        protected ResponseModel Failed(string msg = null)
        {
            return new ResponseModel
            {
                Message = string.IsNullOrEmpty(msg) ? string.Empty : msg,
                Error = true
            };
        }
    }
}