USE [livebroadcast]
GO
/****** Object:  Table [dbo].[AppSettings]    Script Date: 2019/6/3 11:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppSettings](
	[Id] [uniqueidentifier] NOT NULL,
	[MiniProgramAppId] [nvarchar](512) NOT NULL,
	[MiniProgramSecret] [nvarchar](512) NOT NULL,
	[MiniProgramAccessToken] [nvarchar](512) NULL,
	[MiniProgramAccessTokenExpireTime] [datetime] NULL,
	[MiniProgramMchId] [nvarchar](50) NULL,
	[WeXinPaymentApiSecret] [nvarchar](32) NULL,
	[ExtranetIp] [nvarchar](50) NULL,
	[AliyunAppId] [nvarchar](512) NULL,
	[AliyunAppSecret] [nvarchar](512) NULL,
	[OssEndpoint] [nvarchar](50) NULL,
	[BucketName] [nvarchar](50) NULL,
	[SmsSignName] [nvarchar](50) NULL,
	[SmsTemplateCode] [nvarchar](50) NULL,
	[RandomNumberLength] [int] NULL,
	[IntervalTime] [int] NULL,
	[ExpiryTime] [int] NULL,
	[PushDomain] [nvarchar](150) NULL,
	[PlayDomain] [nvarchar](150) NULL,
	[PushSecretKey] [nvarchar](50) NULL,
	[BizId] [nvarchar](50) NULL,
	[RealTimeAudioAndVideoSdkAppId] [nvarchar](50) NULL,
 CONSTRAINT [PK_AppSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 2019/6/3 11:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[Id] [uniqueidentifier] NOT NULL,
	[Context] [nvarchar](max) NULL,
	[CreatedTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoomOnLineUsers]    Script Date: 2019/6/3 11:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoomOnLineUsers](
	[Id] [uniqueidentifier] NOT NULL,
	[RoomId] [uniqueidentifier] NOT NULL,
	[WeChatUserId] [uniqueidentifier] NOT NULL,
	[IsSpeaker] [bit] NOT NULL,
 CONSTRAINT [PK_RoomOnLineUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 2019/6/3 11:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rooms](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[HostWeChatUserUnionId] [nvarchar](50) NULL,
	[HostWeChatUserId] [uniqueidentifier] NULL,
	[AbstractImgUrl] [nvarchar](512) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[State] [nvarchar](50) NULL,
	[DocumentRecordId] [nvarchar](50) NULL,
	[DocumentRecordUrl] [nvarchar](512) NULL,
	[CreatedTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WeChatUsers]    Script Date: 2019/6/3 11:39:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeChatUsers](
	[Id] [uniqueidentifier] NOT NULL,
	[UnionId] [nvarchar](50) NULL,
	[OpenId] [nvarchar](50) NOT NULL,
	[NickName] [nvarchar](50) NULL,
	[AvatarUrl] [nvarchar](512) NULL,
	[Ticket] [nvarchar](50) NULL,
	[Signature] [nvarchar](512) NULL,
	[UpdateTime] [datetime] NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_WeChatUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
