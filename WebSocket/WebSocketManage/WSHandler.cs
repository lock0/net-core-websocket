﻿using System;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocket.models;

namespace WebSocket.WebSocketManage
{
    public class WsHandler

    {
        protected WsConnectionManager WsConnectionManager;


        public WsHandler(WsConnectionManager wSConnectionManager)

        {
            WsConnectionManager = wSConnectionManager;
        }


        public async Task SendMessageAsync(
            CustomWebSocket socket,
            string message,
            CancellationToken cancellationToken = default(CancellationToken))

        {
            var buffer = Encoding.UTF8.GetBytes(message);

            var segment = new ArraySegment<byte>(buffer);


            await socket.WebSocket.SendAsync(segment, WebSocketMessageType.Text, true, cancellationToken);
        }

        /// <summary>
        /// 发送给给所有人
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task SendMessageToAllAsync(string message,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach (var pair in WsConnectionManager.GetAll())
            {
                if (pair.Value.WebSocket.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message, cancellationToken);
            }
        }

        /// <summary>
        /// 发送给相同房间里的人
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task SendMessageToAllByRoomAsync(string roomId, string message,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach (var pair in WsConnectionManager.GetAll().Where(n=>n.Value.RoomId==roomId))
            {
                if (pair.Value.WebSocket.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message, cancellationToken);
            }
        }

        public async Task<string> RecieveAsync(CustomWebSocket webSocket,
            CancellationToken cancellationToken)
        {
            var buffer = new ArraySegment<byte>(new byte[1024 * 8]);

            using (var ms = new MemoryStream())
            {
                WebSocketReceiveResult result;
                do
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    result = await webSocket.WebSocket.ReceiveAsync(buffer, cancellationToken);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                } while (!result.EndOfMessage);


                ms.Seek(0, SeekOrigin.Begin);

                if (result.MessageType != WebSocketMessageType.Text)

                {
                    return null;
                }


                using (var reader = new StreamReader(ms, Encoding.UTF8))

                {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }
}