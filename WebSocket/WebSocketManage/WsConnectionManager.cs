﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocket.models;

namespace WebSocket.WebSocketManage
{
    public class WsConnectionManager
    {
        private static readonly ConcurrentDictionary<string, CustomWebSocket> SocketConcurrentDictionary =
            new ConcurrentDictionary<string, CustomWebSocket>();


        public void AddSocket(CustomWebSocket socket)
        {
            SocketConcurrentDictionary.TryAdd(CreateGuid(), socket);
        }


        public async Task RemoveSocket(CustomWebSocket socket)
        {
            SocketConcurrentDictionary.TryRemove(GetSocketId(socket), out CustomWebSocket aSocket);
            await aSocket.WebSocket.CloseAsync(
                closeStatus: WebSocketCloseStatus.NormalClosure,
                statusDescription: "Close by User",
                cancellationToken: CancellationToken.None).ConfigureAwait(false);
        }


        public string GetSocketId(CustomWebSocket socket)

        {
            return SocketConcurrentDictionary.FirstOrDefault(k => k.Value == socket).Key;
        }


        public ConcurrentDictionary<string, CustomWebSocket> GetAll()

        {
            return SocketConcurrentDictionary;
        }


        public string CreateGuid()

        {
            return Guid.NewGuid().ToString();
        }
    }
}