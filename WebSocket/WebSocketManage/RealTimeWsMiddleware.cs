﻿using System;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using library.services;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebSocket.models;

namespace WebSocket.WebSocketManage
{
    public class RealTimeWsMiddleware
    {
        private readonly RequestDelegate _next;
        private WsConnectionManager WsConnectionManager { get; set; }
        private WsHandler WsHanlder { get; set; }
        private IRoomOnLineUserService RoomOnLineUserService { get; set; }

        public RealTimeWsMiddleware(
            RequestDelegate next,
            WsConnectionManager wSConnectionManager,
            WsHandler wsHandler,
            IRoomOnLineUserService roomOnLineUserService)
        {
            _next = next;
            WsConnectionManager = wSConnectionManager;
            WsHanlder = wsHandler;
            RoomOnLineUserService = roomOnLineUserService;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.WebSockets.IsWebSocketRequest)
            {
                var cancellationToken = httpContext.RequestAborted;
                var roomId = httpContext.Request.Query["roomId"].ToString(); //传递来的参数
                var userId = httpContext.Request.Query["userId"].ToString(); //这里是OpenId
                var userName = httpContext.Request.Query["userName"].ToString();
                var currentWebSocket = await httpContext.WebSockets.AcceptWebSocketAsync();
                var customWebSocket = new CustomWebSocket
                {
                    RoomId = roomId,
                    User = new WebSocketUser
                    {
                        UserName = userName,
                        UserId = userId
                    },
                    WebSocket = currentWebSocket
                };
                WsConnectionManager.AddSocket(customWebSocket); //这里可以捕捉上线的人

                #region 只要有人上线就发送上线通知

                RoomOnLineUserService.OnLine(customWebSocket.User.UserId, new Guid(roomId));
                var joinMessage = new MessageJoinFormat
                {
                    User = customWebSocket.User
                };
                await WsHanlder.SendMessageToAllByRoomAsync(customWebSocket.RoomId,
                    JsonConvert.SerializeObject(joinMessage), cancellationToken);

                #endregion

                while (true)
                {
                    if (cancellationToken.IsCancellationRequested) break;
                    var response = await WsHanlder.RecieveAsync(customWebSocket, cancellationToken);
                    if (string.IsNullOrEmpty(response) && customWebSocket.WebSocket.State != WebSocketState.Open)
                    {
                        #region 只要有人下线就发送离线通知

                        RoomOnLineUserService.OffLine(customWebSocket.User.UserId, new Guid(roomId));
                        var leaveMessage = new MessageLeaveFormat
                        {
                            User = customWebSocket.User
                        };
                        await WsHanlder.SendMessageToAllByRoomAsync(customWebSocket.RoomId,
                            JsonConvert.SerializeObject(leaveMessage), cancellationToken); //这里可以捕捉下线的人

                        #endregion

                        break;
                    }

                    //一般的
                    var otherMessage = new MessageOtherFormat
                    {
                        Context = response
                    };
                    await WsHanlder.SendMessageToAllByRoomAsync(customWebSocket.RoomId,
                        JsonConvert.SerializeObject(otherMessage), cancellationToken);
                }

                await WsConnectionManager.RemoveSocket(customWebSocket);
            }
            else
            {
                await _next(httpContext);
            }
        }
    }
}