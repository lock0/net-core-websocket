﻿using System;
using System.Collections.Generic;
using System.Text;
using library.models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace library.services
{
    public interface IDataContext : IDisposable
    {
        DbSet<AppSetting> AppSettings { get; set; }
        DbSet<WeChatUser> WeChatUsers { get; set; }
        DbSet<Room> Rooms { get; set; }
        DbSet<RoomOnLineUser> RoomOnLineUsers { get; set; }

        int SaveChanges();
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
