﻿using library.models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace library.services
{
    public interface IWeChatUserService : IDisposable
    {
        void Insert(WeChatUser weChatUser);
        void Update();
        WeChatUser GetWeChatUser(Guid id);
        WeChatUser GetWeChatUser(string openId);
        WeChatUser GetWeChatUserByTicket(string ticket);
        IQueryable<WeChatUser> GetWeChatUsers();
    }
}
