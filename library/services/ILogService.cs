﻿using library.models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace library.services
{
    public interface ILogService : IDisposable
    {
        void Insert(Log log);
        void Update();
        Log GetLog(Guid id);
        IQueryable<Log> GetLogs();
    }
}