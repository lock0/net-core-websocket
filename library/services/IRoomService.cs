﻿using library.models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace library.services
{
    public interface IRoomService : IDisposable
    {
        void Insert(Room room);
        void Update();
        Room GetRoom(Guid id);
        IQueryable<Room> GetRooms();
    }
}