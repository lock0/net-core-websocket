﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library.services
{
    /// <summary>
    /// 小程序官方暴露的api
    /// </summary>
    public interface IMiniProgramService
    {
        /// <summary>
        /// 获取小程序码，适用于需要的码数量极多的业务场景。通过该接口生成的小程序码，永久有效，数量暂无限制。 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        dynamic GetWxaCodeUnlimit(string accessToken, dynamic content);

        /// <summary>
        /// 获取小程序全局唯一后台接口调用凭据（access_token）。调调用绝大多数后台接口时都需使用 access_token，开发者需要进行妥善保存。
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        dynamic GetAccessToken(string appid, string secret);

        /// <summary>
        /// 获取模板列表
        /// </summary>
        /// <param name="accessToken">公众号的accessToken</param>
        /// <returns></returns>
        dynamic GetTemplateList(string accessToken);

        /// <summary>
        /// 发送模板消息
        /// </summary>
        /// <param name="accessToken">小程序的accessToken</param>
        /// <param name="content">发送的json内容</param>
        /// <returns></returns>
        dynamic SendTemplateMessage(string accessToken, dynamic content);
    }
}