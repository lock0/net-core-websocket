﻿using library.models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace library.services
{
    public interface IRoomOnLineUserService : IDisposable
    {
        void Insert(RoomOnLineUser roomOnLineUser);
        void Update();
        RoomOnLineUser GetRoomOnLineUser(Guid id);
        IQueryable<RoomOnLineUser> GetRoomOnLineUsers();
        void OnLine(string openId,Guid roomId );
        void OffLine(string openId, Guid roomId);
    }
}