﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using library.models;

namespace library.services
{
    public interface IAppSettingService : IDisposable
    {
        void Insert(AppSetting appSetting);
        void Update();
        AppSetting GetAppSetting(Guid id);
        IQueryable<AppSetting> GetAppSettings();
        /// <summary>
        /// 包含最新有效的小程序Token
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        AppSetting GetAppSettingIncludeLastToken(Guid id);
        AppSetting GetAppSettingIncludeLastToken(string miniProgramAppId);
    }
}
