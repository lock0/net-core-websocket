﻿using System;
using System.Collections.Generic;
using System.Text;

namespace library.interfaces
{
    public interface IDtStamped
    {
        DateTime UpdateTime { get; set; }
        DateTime CreatedTime { get; set; }
        bool IsDeleted { get; set; }
    }
}
