﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using library.interfaces;

namespace library.models
{
    [Table("WeChatUsers")]
    public class WeChatUser : IDtStamped
    {
        [Key] public Guid Id { get; set; }
        public string UnionId { get; set; }
        public string OpenId { get; set; }
        public string NickName { get; set; }
        public string AvatarUrl { get; set; }
        public string Ticket { get; set; }
        /// <summary>
        /// 实时音视频签名UserSig
        /// </summary>
        public string Signature { get; set; }
        public DateTime UpdateTime { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}