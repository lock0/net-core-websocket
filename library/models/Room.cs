﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using library.interfaces;

namespace library.models
{
    [Table("Rooms")]
    public class Room : IDtStamped
    {
        [Key]
        public Guid Id { get; set; }
        public string Code { get; set; }
        public Guid? HostWeChatUserId { get; set; }
        [ForeignKey("HostWeChatUserId")]
        public virtual WeChatUser Host { get; set; }
        public string HostWeChatUserUnionId { get; set; }
        /// <summary>
        /// 摘要图片
        /// </summary>
        public string AbstractImgUrl { get; set; }
        /// <summary>
        /// 直播开始时间
        /// </summary>
        public DateTime? StartDateTime { get;set; }
        /// <summary>
        /// 直播结束时间
        /// </summary>
        public DateTime? EndDateTime { get; set; }
        /// <summary>
        /// 会议状态
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// 当前播放文档的Id
        /// </summary>
        public string DocumentRecordId { get; set; }
        /// <summary>
        /// 当前播放文档的URL
        /// </summary>
        public string DocumentRecordUrl { get; set; }
        public DateTime UpdateTime { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool IsDeleted { get; set; }
        public virtual  ICollection<RoomOnLineUser> RoomOnLineUsers { get; set; }
    }
}
