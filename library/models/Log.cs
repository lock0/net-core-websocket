﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using library.interfaces;

namespace library.models
{
    [Table("Logs")]
    public class Log : IDtStamped
    {
        [Key]
        public Guid Id { get; set; }
        public string Context { get; set; }
        public DateTime UpdateTime { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
