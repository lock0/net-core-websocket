﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library.models
{
    /// <summary>
    /// 基础配置 小程序 阿里云
    /// </summary>
    [Table("AppSettings")]
    public class AppSetting
    {
        [Key] public Guid Id { get; set; }

        /// <summary>
        /// 小程序AppId
        /// </summary>
        public string MiniProgramAppId { get; set; }

        /// <summary>
        /// 小程序Secret
        /// </summary>

        public string MiniProgramSecret { get; set; }

        /// <summary>
        /// 腾讯直播推流域名
        /// </summary>
        public string PushDomain { get; set; }

        /// <summary>
        /// 腾讯直播播放域名
        /// </summary>
        public string PlayDomain { get; set; }

        /// <summary>
        /// 腾讯直播推流防盗链Key
        /// </summary>
        public string PushSecretKey { get; set; }
        /// <summary>
        /// 腾讯直播BizId
        /// </summary>
        public string BizId { get; set; }
        /// <summary>
        /// 实时音视频的SdkAppId
        /// </summary>
        public string RealTimeAudioAndVideoSdkAppId { get; set; }
        /// <summary>
        /// 微信商户Id
        /// </summary>
        public string MiniProgramMchId { get; set; }

        /// <summary>
        /// 外网IP
        /// </summary>
        public string ExtranetIp { get; set; }

        /// <summary>
        /// 微信支付API调用密钥
        /// </summary>
        public string WeXinPaymentApiSecret { get; set; }

        /// <summary>
        /// 小程序Token
        /// </summary>
        public string MiniProgramAccessToken { get; set; }

        /// <summary>
        /// 小程序Token到期时间
        /// </summary>
        public DateTime? MiniProgramAccessTokenExpireTime { get; set; }

        /// <summary>
        /// 阿里云
        /// </summary>
        public string AliyunAppId { get; set; }

        public string AliyunAppSecret { get; set; }

        /// <summary>
        /// oss地址
        /// </summary>
        public string OssEndpoint { get; set; }

        /// <summary>
        /// 桶名
        /// </summary>
        public string BucketName { get; set; }

        /// <summary>
        /// 短信签名
        /// </summary>
        public string SmsSignName { get; set; }

        /// <summary>
        /// 短信模板Code
        /// </summary>
        public string SmsTemplateCode { get; set; }

        /// <summary>
        /// 随机数长度
        /// </summary>
        public int? RandomNumberLength { get; set; }

        /// <summary>
        /// 验证码获取最大间隔秒数
        /// </summary>
        public int? IntervalTime { get; set; }

        /// <summary>
        /// 验证码过期秒数
        /// </summary>
        public int? ExpiryTime { get; set; }
    }
}