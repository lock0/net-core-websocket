﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace library.models
{
    [Table("RoomOnLineUsers")]
    public class RoomOnLineUser
    {
        [Key]
        public Guid Id { get; set; }
        public Guid RoomId { get; set; }
        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
        public Guid WeChatUserId { get; set; }
        [ForeignKey("WeChatUserId")]
        public virtual WeChatUser WeChatUser { get; set; }
        /// <summary>
        /// 是否是当前的讲者
        /// </summary>
        public bool IsSpeaker { get; set; }
    }
}
